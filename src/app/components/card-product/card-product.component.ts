import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card-product',
  templateUrl: './card-product.component.html',
  styleUrls: ['./card-product.component.css']
})
export class CardProductComponent {
  @Input() productId: number = 0;
  @Input() productName: string = "Product name";
  @Input() productDescription: string = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi ad vel distinctio temporibus accusantium asperiores, sunt mollitia. Modi, corrupti sed!";
}
