import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './router/home/home.component';
import { LoginComponent } from './router/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { ProductsComponent } from './router/products/products.component';
import { CardProductComponent } from './components/card-product/card-product.component';
import { NotFoundComponent } from './router/not-found/not-found.component';
import { DetailedProductComponent } from './router/detailed-product/detailed-product.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    NavbarComponent,
    FooterComponent,
    ProductsComponent,
    CardProductComponent,
    NotFoundComponent,
    DetailedProductComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
